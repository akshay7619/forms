import { Component } from '@angular/core';

import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'form';
  dataFromChild:String;
  // public message='';
  // message($event) {
  //   console.log($event);
  // }



  handleNotify(eventData:String){
    this.dataFromChild=eventData;
    console.log(eventData);
  }
}
