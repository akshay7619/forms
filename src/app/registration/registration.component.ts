import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
  
  @Input() public  parentData;



  rForm: FormGroup;
  post: any;
  description: string = '';
  name1: string = "";
  titleAlert: string = "This is Required !"

  constructor(private fb: FormBuilder) {

    this.rForm = fb.group({
      'name1': [null, Validators.required],
      'description': [null, Validators.compose([Validators.required, Validators.minLength(30), Validators.maxLength(500)])],
      'validate': ''

    });
  }

  // addPost(post) {
  //   this.description = post.description;
  //   this.name = post.name;
  // }

  ngOnInit() {
  }

}
